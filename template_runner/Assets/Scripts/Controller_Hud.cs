﻿using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    public static bool gameOver = false;
    public Text distanceText;
    public Text gameOverText;
    private float distance = 0;
    int valorDistancia;

    void Start()
    {
        
        gameOver = false;
        distance = 0;
        distanceText.text = valorDistancia.ToString();
        gameOverText.gameObject.SetActive(false);
    }

    void Update()
    {
        //Convierto distance(float) en entero
        valorDistancia = (int)distance;
        if (gameOver)
        {
            Time.timeScale = 0;
            gameOverText.text = "Game Over \n Total Distance: " + valorDistancia.ToString();
            gameOverText.gameObject.SetActive(true);
        }
        else
        {
            distance += Time.deltaTime;
            distanceText.text = valorDistancia.ToString();
        }
    }
}
